---

<p align="center">
<img src="https://raw.githubusercontent.com/AHAAAAAAA/PokemonGo-Map/master/static/cover.png">
</p>

# Pokémon GO Radar


---

Auto Installer

---

  bash install or ./install

---

Running

---

To start the server, run the following command:

  python run.py -u [YOUR EMAIL] -p [PASSWORD] -st 10

---

Autorun

---

  bash x or ./x

Please edit file x

---

Replacing [YOUR EMAIL] and [PASSWORD] with the Pokemon Club credentials you
created previously, and [LOCATION] with any location, for example KEDIRI or latitude and longitude coordinates, such as -7.815908 112.062256 .

Additionally, you can change the 10 after -st to any number. This number
indicates the number of steps away from your location it should look, higher numbers
being farther.

---

Accessing

---

Open your browser to http://localhost:5000 and keep refreshing as it loads
more Pokemon (auto refresh is not implemented yet).

----